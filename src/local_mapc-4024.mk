# Include directory of Gnu Scientific Library (GSL)
GSL_INCLUDE_DIR=/home/em459/Library/gsl-2.5/include/gsl

# Lib directory of Gnu Scientific Library (GSL)
GSL_LIB_DIR=/home/em459/Library/gsl-2.5/lib/

# Directory of eigen library
EIGEN_INCLUDE_DIR=/home/em459/Library/eigen-3.3.4/include/eigen3/

# Additional linker flags
LOCAL_LFLAGS=

# Compile in debugging mode (set to True, if required)?
DEBUG=False

# Use MPI?
USE_MPI=False

# MPI compiler
MPICXX=mpic++