# Include directory of Gnu Scientific Library (GSL)
GSL_INCLUDE_DIR=/usr/local/include/

# Lib directory of Gnu Scientific Library (GSL)
GSL_LIB_DIR=/usr/local/lib/

# Directory of eigen library
EIGEN_INCLUDE_DIR=/usr/local/include/eigen3/

# Additional linker flags
LOCAL_LFLAGS=

# Compile in debugging mode (set to True, if required)?
DEBUG=False

# Use MPI?
USE_MPI=False

# MPI compiler
MPICXX=mpic++