# Include directory of Gnu Scientific Library (GSL)
GSL_INCLUDE_DIR=/apps/gsl/2.1/include/gsl
# Lib directory of Gnu Scientific Library (GSL)
GSL_LIB_DIR=/apps/gsl/2.1/lib/

# Directory of eigen library
EIGEN_INCLUDE_DIR=/home/n/em459/scratch/library/eigen-3.3.7/include/eigen3/

# Additional linker flags
LOCAL_LFLAGS=

# Compile in debugging mode (set to True, if required)?
DEBUG=False

# Use MPI?
USE_MPI=True

# MPI compiler
MPICXX=mpicxx