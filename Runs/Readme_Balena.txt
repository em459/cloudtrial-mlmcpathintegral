Use the following module files (you can source setup_balena.bash for this):

module purge
module load slurm
module load intel/compiler/64/18.5.274
module load intel/mpi
module load intel/mkl
module load gsl

Prerequisites
=============
The Eigen library has to be installed before compiling the code. This is a C++ template library, which can be obtained from http://eigen.tuxfamily.org. Install this into a local directory, say 

${HOME}/scratch/eigen-3.3.7/include/eigen3/

The gnu scientific library (gsl) is also required.

Compilation
===========
(1) Go to the src subdirectory

(2) Copy the file local_balena.mk to local.mk, and edit the file accordingly. In particular, set the path to the include files for the eigen library, such as EIGEN_INCLUDE_DIR=${HOME}/scratch/eigen-3.3.7/include/eigen3/. You might also have to change the gsl directories, if the gsl is installed in a different location.

(3) type 'make', this should build the source and produce an executable called 'driver.x'

Running
=======
(1) Go to the directory Source (or copy the executable 'driver.x' somewhere else)

(2) to run the code on 16 cores with a parameter file called 'parameters.in', use

   mpirun -n 16 ./driver.x parameters.in

(3) Timing output will be printedproduced to the screeen, the line to look out for is 

[timer SinglevelMC] : 1.1736e+02 s

Parameter files
===============
The current directory contains a file parameters.in which should be used for the runs. When running on 16 cores, a run should only take a few minutes (less than 5).

Balena jobscript
================
Use the slurm script 'jobscript_balena.slm' to carry out runs. All run data will be stored in a directory which is created by the jobscript. Before using the job script, obviously compile the executable, which the script assumes to be in ../src.

Upon completion, output is written to a file of the form slurm-JOBID.out and the output is in the directory mlmcpathintegral_JOBID, e.g.:

[em459@balena-01 Runs]$ ls slurm-*out
slurm-2763750.out
[em459@balena-01 Runs]$ ls mlmcpathintegral_2763750/
driver.x  output.txt  parameters.in  slurm_script

Timing information can be extracted from standard output in the file output.txt in the run directory.
