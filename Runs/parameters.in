###########################################
# General parameters
###########################################
general:
  do_singlelevelmc = true # Run single level MC algorithm?
  do_twolevelmc = false # Run two level MC algorithm?
  do_multilevelmc = false # Run multilevel MC algorithm?
  action = 'quarticoscillator' # action to use. Allowed values are:
                                # [harmonicoscillator,
                                #  quarticoscillator,
                                #  doublewell,
                                #  rotor]

###########################################
# Lattice parameters
###########################################
lattice:
  M_lat = 256        # Number of lattice sites
  T_final = 4.0     # Final time

###########################################
# Statistics parameters
###########################################
statistics:
  n_autocorr_window = 20   # Size of window over which to measure
                           # autocorrelations
  n_min_samples_qoi = 100  # Minimal number of samples for uncorrelated
                           # estimators

###########################################
# Harmonic oscillator action parameters
###########################################
harmonicoscillator:
  m0 = 1.0           # Unrenormalised mass
  mu2 = 1.0          # Curvature of potential
  renormalisation = 'none' # Renormalisation [none, perturbative, exact]

###########################################
# Double well action parameters
###########################################
doublewell:
  m0 = 1.0           # Unrenormalised mass
  mu2 = 1.0          # Curvature of potential
  lambda = 1.0       # Parameter lambda
  sigma = 1.0        # Parameter sigma

###########################################
# Quartic oscillator action parameters
###########################################
quarticoscillator:
  m0 = 1.0           # Unrenormalised mass
  mu2 = -1.0          # Curvature of potential
  lambda = 1.0       # Parameter lambda
  x0 = 0.25

###########################################
# Rotor action parameters
###########################################
rotor:
  m0 = 0.25          # Unrenormalised mass
  renormalisation = 'none' # Renormalisation [none, perturbative]
  
###########################################
# Single level Monte Carlo parameters
###########################################
singlelevelmc:
  n_burnin = 10000     # Number of burnin samples
  n_samples = 10000000      # Number of samples, set to 0 to use epsilon instead 
  epsilon = 1.0E-2   # Tolerance epsilon
  sampler = 'HMC'    # Sampler to use [HMC, cluster, exact, hierarchical]

###########################################
# Two level Monte Carlo parameters
###########################################
twolevelmc:
  n_burnin = 100      # Number of burnin samples
  n_samples = 1000    # Number of samples
  coarsesampler = 'HMC' # Sampler to use [HMC, cluster, exact]

###########################################
# Multilevel Monte Carlo parameters
###########################################
multilevelmc:
  n_level = 3      # Number of levels
  n_burnin = 100      # Number of burnin samples
  epsilon = 1.0    # Tolerance epsilon
  show_detailed_stats = false  # Print out detailed statistics?

###########################################
# Hierarchical sampler parameters
###########################################
hierarchical:
  n_level = 3           # Number of levels
  coarsesampler = 'HMC' # Sampler to use [HMC, cluster, exact]

###########################################
# HMC sampler parameters
###########################################
hmc:
  nt = 100        # Number of HMC steps
  dt = 0.0335       # HMC time step
  n_burnin = 1000  # Number of burnin samples

###########################################
# Cluster sampler parameters
###########################################
clusteralgorithm:
  n_burnin = 100  # Number of burnin samples
  n_updates = 10  # Number of cluster updates between steps

#
