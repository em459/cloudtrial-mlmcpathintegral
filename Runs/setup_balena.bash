module purge
module load slurm
module load intel/compiler/64/18.5.274
module load intel/mpi
module load intel/mkl
module load gsl
